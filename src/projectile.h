#pragma once
#include <raylib.h>

#define PROJECTILE_MAX   12
#define PROJECTILE_SPEED 500
#define PROJECTILE_LIFE  2.0f
#define PROJECTILE_THICK 10.0f
#define PROJECTILE_LEN   30.0f
#define PROJECTILE_COLOR   \
    CLITERAL (Color)       \
    {                      \
        245, 201, 207, 255 \
    }

typedef struct {
    bool active;

    Vector2 position;
    float rotation;
    float creationTime;
} Projectile;

Projectile createProjectile (Vector2 position, float rotation);
bool projectileUpdate (Projectile *projectile, float frametime, float time);
void projectileDraw (Projectile projectile);
void addProjectile (Vector2 position, float rotation);
int updateProjectiles (void);
void drawProjectiles (void);
void resetProjectiles (void);
