#include "enemy.h"
#include "config.h"
#include <raylib.h>
#include <raymath.h>
#include "score.h"
#include <string.h>
#include "../assets/enemy1.png.h"
#include "../assets/enemy2.png.h"
#include "../assets/enemy3.png.h"
#include "../assets/enemy4.png.h"

static enemySize _sizes[]           = {ENEMY_SMALL, ENEMY_MEDIUM, ENEMY_LARGE};
static Enemy _enemies[ENEMY_MAX]    = {0};
static float _lastEnemyCreationTime = -1.0f;
static float _newEnemyBaseAngle     = 0;
#define MAX_TEXTURE 4
static Texture2D textures[MAX_TEXTURE] = {0};

Texture2D LoadTextureFromHeader (int width, int height, PixelFormat format, void *data)
{
    Image img;
    img.height  = height;
    img.width   = width;
    img.mipmaps = 1;
    img.format  = format;
    img.data    = data;
    return LoadTextureFromImage (img);
}

void initEnemy (void)
{
    textures[0] = LoadTextureFromHeader (ENEMY1_WIDTH, ENEMY1_HEIGHT, ENEMY1_FORMAT, ENEMY1_DATA);
    textures[1] = LoadTextureFromHeader (ENEMY2_WIDTH, ENEMY2_HEIGHT, ENEMY2_FORMAT, ENEMY2_DATA);
    textures[2] = LoadTextureFromHeader (ENEMY3_WIDTH, ENEMY3_HEIGHT, ENEMY3_FORMAT, ENEMY3_DATA);
    textures[3] = LoadTextureFromHeader (ENEMY4_WIDTH, ENEMY4_HEIGHT, ENEMY4_FORMAT, ENEMY4_DATA);
}

Enemy createEnemy (Vector2 position, Vector2 velocity, enemySize size)
{
    return (Enemy) {.active        = true,
                    .pos           = position,
                    .vel           = velocity,
                    .size          = size,
                    .texture       = &textures[GetRandomValue (0, MAX_TEXTURE - 1)],
                    .rotation      = GetRandomValue (0, 360),
                    .rotationSpeed = GetRandomValue (ENEMY_ROT_SPEED_MIN, ENEMY_ROT_SPEED_MAX),
                    .creationTime  = GetTime ()};
}

bool enemyUpdate (Enemy *enemy, float frametime, float time)
{
    if (! enemy->active) {
        return false;
    }

    if (time > enemy->creationTime + ENEMY_LIFE) {
        enemy->active = false;
        return false;
    }

    enemy->pos = Vector2Add (enemy->pos, Vector2Scale (enemy->vel, frametime));
    enemy->rotation += enemy->rotationSpeed * frametime;

    return true;
}

void enemyDraw (Enemy *enemy)
{
    if (! enemy->active) {
        return;
    }
    Rectangle source = {0, 0, enemy->texture->width, enemy->texture->height};
    Rectangle dest   = {enemy->pos.x, enemy->pos.y, 3 * enemyRadius (enemy), 3 * enemyRadius (enemy)};
    Vector2 origin   = {dest.width / 2, dest.height / 2};
#ifdef DEBUG
    DrawCircleLinesV (enemy->pos, (64 * enemy->size) / 2, RED);
#endif
    DrawTexturePro (*enemy->texture, source, dest, origin, enemy->rotation, WHITE);
}

float enemyRadius (Enemy *enemy)
{
    return 16.0f * (int) enemy->size;
}

void addEnemy (Vector2 position, enemySize size, float speedMod, bool spawn)
{
    float tweakAngle = spawn ? ENEMY_RANDOM_ANGLE : ENEMY_SPLIT_ANGLE;

    Vector2 velocity = spawn ? Vector2Subtract (WIN_CENTRE, position)
                             : Vector2Rotate ((Vector2) {0, -1}, _newEnemyBaseAngle * DEG2RAD);
    velocity = Vector2Scale (Vector2Normalize (velocity), speedMod * GetRandomValue (ENEMY_SPEED_MIN, ENEMY_SPEED_MAX));

    velocity = Vector2Rotate (velocity, (float) GetRandomValue (-tweakAngle, tweakAngle));

    for (int i = 0; i < ENEMY_MAX; i++) {
        if (_enemies[i].active) {
            continue;
        }

        _enemies[i] = createEnemy (position, velocity, size);
        break;
    }
}

void destroyEnemy (int index, float angle)
{
    const float largeSpeedMod  = 0.5f;
    const float mediumSpeedMod = 0.33f;

    Enemy *enemy  = _enemies + index;
    enemy->active = false;

    _newEnemyBaseAngle = angle;
    int points         = ENEMY_BASE_SCORE * (int) enemy->size;
    switch (enemy->size) {
    case ENEMY_LARGE:
        addEnemy (_enemies->pos, ENEMY_MEDIUM, largeSpeedMod, false);
        addEnemy (_enemies->pos, ENEMY_MEDIUM, largeSpeedMod, false);
        break;
    case ENEMY_MEDIUM:
        addEnemy (_enemies->pos, ENEMY_SMALL, mediumSpeedMod, false);
        addEnemy (_enemies->pos, ENEMY_SMALL, mediumSpeedMod, false);
        break;
    default:
        _newEnemyBaseAngle = 0;
        break;
    }

    if (points <= 0) {
        return;
    }

    addScore (points);
}

Vector2 getNextEnemyPosition (void)
{
    int padding    = 128;
    Vector2 result = {-padding, -padding};

    if (GetRandomValue (0, 1)) {
        if (GetRandomValue (0, 1)) {
            result.y = WIN_HEIGHT + padding;
        }

        result.x = GetRandomValue (-padding, WIN_WIDTH + padding);
    } else {
        if (GetRandomValue (0, 1)) {
            result.x = WIN_WIDTH + padding;
        }

        result.y = GetRandomValue (-padding, WIN_HEIGHT + padding);
    }

    return result;
}

int updateEnemys (void)
{
    int activeAsteroids = 0;

    float frametime = GetFrameTime ();
    float _time     = GetTime ();

    for (int i = 0; i < ENEMY_MAX; i++) {
        if (enemyUpdate (_enemies + i, frametime, _time)) {
            activeAsteroids++;
        }
    }

    if (_time > _lastEnemyCreationTime + ENEMY_DELAY) {
        enemySize nextSize = _sizes[GetRandomValue (0, 2)];
        addEnemy (getNextEnemyPosition (), nextSize, 1.0f, true);
        _lastEnemyCreationTime = _time;
    }

    return activeAsteroids;
}

void drawEnemys (void)
{
    for (int i = 0; i < ENEMY_MAX; i++) {
        enemyDraw (&_enemies[i]);
    }
}

Enemy *enemiesArray (void)
{
    return _enemies;
}

void resetEnemys (void)
{
    memset (_enemies, 0, sizeof (_enemies));
    _lastEnemyCreationTime = -1.0f;
    _newEnemyBaseAngle     = 0;
}
