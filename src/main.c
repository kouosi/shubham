#include <stdlib.h>
#include <time.h>
#include <raylib.h>
#include "config.h"
#include "game.h"

int main (void)
{
    srand (time (0));
    InitWindow (WIN_WIDTH, WIN_HEIGHT, WIN_TITLE);
    initGame ();
#ifndef DEBUG
    SetTargetFPS (60);
#endif
    // Main game loop
    while (! WindowShouldClose ()) {
        updateGame ();
        BeginDrawing ();
        ClearBackground (RAYWHITE);
        drawGame ();
#ifdef DEBUG
        DrawFPS (4, 4);
#endif
        EndDrawing ();
    }
    // Close Window when done
    CloseWindow ();
    return 0;
}
