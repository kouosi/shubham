#pragma once

#include <raylib.h>
#include "config.h"

typedef enum {
    PLAYER_DEFAULT,
    PLAYER_STUNNED,
    PLAYER_IFRAME,
    PLAYER_DEAD
} playerState;

#define PLAYER_HEALTH_MAX 5
#define PLAYER_FIRE_DELAY 0.33f
#define PLAYER_RADIUS     32

typedef struct {
    Vector2 pos;
    Vector2 vel;
    playerState state;
    float timeStateEntered;
    float rotation;
    float lastFireTime;
    int health;
} Player;

#define PLAYER_ROT_SPEED         360
#define PLAYER_SPEED             250
#define PLAYER_ACCELERATION      750
#define PLAYER_DECELERATION      175
#define PLAYER_NUDGE_VELOCITY    150
#define PLAYER_PROJECTILE_OFFSET PLAYER_RADIUS
#define FIELD_MIN_X              (-PLAYER_RADIUS / 2)
#define FIELD_MAX_X              (WIN_WIDTH + PLAYER_RADIUS / 2)
#define FIELD_MIN_Y              (-PLAYER_RADIUS / 2)
#define FIELD_MAX_Y              (WIN_HEIGHT + PLAYER_RADIUS / 2)

void resetPlayer (void);
void initPlayer (void);
void drawPlayer (void);
void playerSetState (playerState state);
int playerHealth (void);
void movePlayer (void);
Vector2 playerFacingDirection (void);
void updatePlayer (void);
