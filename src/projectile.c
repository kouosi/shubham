#include "projectile.h"
#include "config.h"
#include "enemy.h"
#include <raymath.h>

static Projectile _projectiles[PROJECTILE_MAX];

Projectile createProjectile (Vector2 position, float rotation)
{
    return (Projectile) {.position = position, .rotation = rotation, .active = true, .creationTime = GetTime ()};
}

bool projectileUpdate (Projectile *projectile, float frametime, float time)
{
    if (! projectile->active) {
        return false;
    }

    if (time > projectile->creationTime + PROJECTILE_LIFE ||
        ! CheckCollisionPointRec (projectile->position, WIN_AREA)) {
        projectile->active = false;
        return false;
    }

    double radians = DEG2RAD * (projectile->rotation - 90.0f);
    projectile->position.x += PROJECTILE_SPEED * cos (radians) * frametime;
    projectile->position.y += PROJECTILE_SPEED * sin (radians) * frametime;

    return true;
}

void projectileDraw (Projectile projectile)
{
    if (! projectile.active) {
        return;
    }

    Rectangle rect = {projectile.position.x, projectile.position.y, PROJECTILE_THICK, PROJECTILE_LEN};
    Vector2 origin = {rect.width / 2, rect.height / 2};
    DrawRectanglePro (rect, origin, projectile.rotation, PROJECTILE_COLOR);
}

void addProjectile (Vector2 position, float rotation)
{
    bool created = false;

    for (int i = 0; i < PROJECTILE_MAX; i++) {
        if (_projectiles[i].active) {
            continue;
        }

        _projectiles[i] = createProjectile (position, rotation);
        created         = true;
        break;
    }

    if (! created) {
        TraceLog (LOG_ERROR, "Failed to create a projectile because there was no inactive spots in the array!");
    }
}

static bool checkCollisionProjectile (Projectile *projectile, Enemy *enemy)
{
    return (! enemy->active) ? false
                             : CheckCollisionPointCircle (projectile->position, enemy->pos, enemyRadius (enemy));
}

int updateProjectiles (void)
{
    float frametime = GetFrameTime ();
    float time      = GetTime ();

    Enemy *enemies = enemiesArray ();

    int projectileCount = 0;
    for (int i = 0; i < PROJECTILE_MAX; i++) {
        Projectile *projectile = _projectiles + i;
        if (projectileUpdate (projectile, frametime, time)) {
            projectileCount++;

            for (int j = 0; j < ENEMY_MAX; j++) {
                Enemy *enemy = enemies + j;
                if (checkCollisionProjectile (projectile, enemy)) {
                    destroyEnemy (j, projectile->rotation);
                    projectile->active = false;
                    // points earned?
                    break;
                }
            }
        }
    }

    return projectileCount;
}

void drawProjectiles (void)
{
    for (int i = 0; i < PROJECTILE_MAX; i++) {
        projectileDraw (_projectiles[i]);
    }
}

void resetProjectiles (void)
{
    for (int i = 0; i < PROJECTILE_MAX; i++) {
        _projectiles[i] = (Projectile) {0};
    }
}
