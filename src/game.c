#include "game.h"
#include "config.h"
#include "player.h"
#include <raylib.h>
#include <stdlib.h>
#include "enemy.h"
#include "score.h"
#include "projectile.h"
#include "gui.h"
#include "raygui.h"

static int _activeAsteroids   = 0;
static int _activeProjectiles = 0;
static GameState _state;

void initGame (void)
{
    initPlayer ();
    initEnemy ();
}

static void setState (GameState state)
{
    switch (state) {
    case GAME_PLAYING:
        resetEnemys ();
        resetPlayer ();
        resetProjectiles ();
        resetScore ();
        break;
    case GAME_MAIN_MENU:
    case GAME_OVER:
    default:
        break;
    }

    _state = state;
}

void updateGame (void)
{
    _activeProjectiles = updateProjectiles ();
    _activeAsteroids   = updateEnemys ();
    updatePlayer ();
}

void drawGame (void)
{
    const int buttonWidth        = 200;
    const int buttonHeight       = 80;
    const Rectangle topButton    = {WIN_CENTRE.x - buttonWidth / 2, WIN_CENTRE.y - buttonHeight / 2 - 5, buttonWidth,
                                    buttonHeight};
    const Rectangle bottomButton = {WIN_CENTRE.x - buttonWidth / 2, WIN_CENTRE.y + buttonHeight / 2 + 5, buttonWidth,
                                    buttonHeight};

    switch (_state) {
    case GAME_MAIN_MENU:
        if (GuiButton (topButton, "PlAY!")) {
            setState (GAME_PLAYING);
            return;
        }

        if (GuiButton (bottomButton, "QUIT")) {
            CloseWindow ();
            exit (0);
            return;
        }
        break;

    case GAME_PLAYING:
        drawEnemys ();
        drawPlayer ();
        drawProjectiles ();
        drawScore ();
        drawHealth ();
        break;

    case GAME_OVER:
        drawEnemys ();

        DrawRectangleRec (WIN_AREA, Fade (BLACK, 0.5f));

        const int fontSize   = 64;
        const char *gameover = "GAME OVER!";
        float measure        = MeasureText (gameover, fontSize);
        DrawText (gameover, WIN_CENTRE.x - measure / 2, fontSize * 1.5f, fontSize, WHITE);

        drawScore ();

        if (GuiButton (topButton, "PLAY AGAIN?")) {
            setState (GAME_PLAYING);
            return;
        }

        if (GuiButton (bottomButton, "QUIT")) {
            CloseWindow ();
            exit (0);
            return;
        }
        break;
    }
}

void gameOver (void)
{
    setState (GAME_OVER);
}
