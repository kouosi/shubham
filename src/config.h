#pragma once

#define WIN_WIDTH  800
#define WIN_HEIGHT 600
#define WIN_TITLE  "Naruto Game by Shubham Pokhrel"
#define WIN_CENTRE                                    \
    (Vector2)                                         \
    {                                                 \
        (float) WIN_WIDTH / 2, (float) WIN_HEIGHT / 2 \
    }
#define WIN_AREA                    \
    (Rectangle)                     \
    {                               \
        0, 0, WIN_WIDTH, WIN_HEIGHT \
    }
#define ENEMY_MAX          164
#define ENEMY_RANDOM_ANGLE 20 * DEG2RAD
#define ENEMY_SPLIT_ANGLE  80 * DEG2RAD
#define ENEMY_DELAY        2.25f
