#pragma once

typedef enum GameState {
    GAME_MAIN_MENU = 0,
    GAME_PLAYING,
    GAME_OVER
} GameState;

void initGame (void);
void updateGame (void);
void drawGame (void);
void gameOver (void);
