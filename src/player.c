#include "player.h"
#include "projectile.h"
#include "config.h"
#include "enemy.h"
#include "game.h"
#include <raylib.h>
#include <raymath.h>
#include "../assets/player.png.h"
#include "../assets/sound.wav.h"

Player _player;
Texture2D _playerTexture;
Sound _sound;

void initPlayer (void)
{
    resetPlayer ();
    _playerTexture = LoadTextureFromHeader (PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_FORMAT, PLAYER_DATA);
    InitAudioDevice ();
    Wave _wave;
    _wave.frameCount = SOUND_FRAME_COUNT;
    _wave.data       = SOUND_DATA;
    _wave.channels   = SOUND_CHANNELS;
    _wave.sampleRate = SOUND_SAMPLE_RATE;
    _wave.sampleSize = SOUND_SAMPLE_SIZE;
    _sound           = LoadSoundFromWave (_wave);
}

void resetPlayer (void)
{
    _player.pos              = WIN_CENTRE;
    _player.vel              = (Vector2) {0};
    _player.state            = PLAYER_DEFAULT;
    _player.rotation         = 0.0f;
    _player.lastFireTime     = -1.0f;
    _player.timeStateEntered = 0.0f;
    _player.health           = PLAYER_HEALTH_MAX;
}

static void onDeath (void)
{
    playerSetState (PLAYER_DEAD);
}

void drawPlayer (void)
{
    if (_player.state == PLAYER_DEAD) {
        return;
    }

    const Rectangle source = {0, 0, _playerTexture.width, _playerTexture.height};
    Rectangle dest         = {_player.pos.x, _player.pos.y, PLAYER_RADIUS * 2, PLAYER_RADIUS * 2};
    Vector2 origin         = {dest.width / 2, dest.height / 2};

    Color color = WHITE;
    if (_player.state == PLAYER_IFRAME) {
        float seconds = GetTime () - _player.timeStateEntered;
        int value     = (int) (seconds * 6.0f);
        if (value % 2) {
            color = Fade (RED, 0.6f);
        }
    }

#ifdef DEBUG
    DrawCircleLinesV (_player.pos, PLAYER_RADIUS, BLUE);
#endif
    DrawTexturePro (_playerTexture, source, dest, origin, _player.rotation, color);
}

void playerSetState (playerState state)
{
    _player.state            = state;
    _player.timeStateEntered = GetTime ();
}

static void tickState (void)
{
    const float stunDuration     = 0.2f;
    const float iframeDuration   = 0.8f;
    const float playerDeathDelay = 0.8f;

    switch (_player.state) {
    case PLAYER_DEFAULT:
        // no check
        break;

    case PLAYER_STUNNED:
        if ((GetTime () - _player.timeStateEntered) > stunDuration) {
            playerSetState (PLAYER_IFRAME);
        }
        break;

    case PLAYER_IFRAME:
        if ((GetTime () - _player.timeStateEntered) > iframeDuration) {
            playerSetState (PLAYER_DEFAULT);
        }
        break;

    case PLAYER_DEAD:
        if ((GetTime () - _player.timeStateEntered) > playerDeathDelay) {
            gameOver ();
        }
        break;

    default:
        break;
    }
}

int playerHealth (void)
{
    return _player.health;
}

static void updateAngle (float frametime)
{
    int xIn = (int) IsKeyDown (KEY_RIGHT) - (int) IsKeyDown (KEY_LEFT);
    _player.rotation += (xIn * PLAYER_ROT_SPEED * frametime);
}

static void updateWrap ()
{
    if (_player.pos.x > FIELD_MAX_X) {
        _player.pos.x = FIELD_MIN_X;

        if (_player.vel.x < PLAYER_NUDGE_VELOCITY) {
            _player.vel.x = PLAYER_NUDGE_VELOCITY;
        }
    } else if (_player.pos.x < FIELD_MIN_X) {
        _player.pos.x = FIELD_MAX_X;

        if (_player.vel.x > -PLAYER_NUDGE_VELOCITY) {
            _player.vel.x = -PLAYER_NUDGE_VELOCITY;
        }
    }

    if (_player.pos.y > FIELD_MAX_Y) {
        _player.pos.y = FIELD_MIN_Y;

        if (_player.vel.y < PLAYER_NUDGE_VELOCITY) {
            _player.vel.y = PLAYER_NUDGE_VELOCITY;
        }
    } else if (_player.pos.y < FIELD_MIN_Y) {
        _player.pos.y = FIELD_MAX_Y;

        if (_player.vel.y > -PLAYER_NUDGE_VELOCITY) {
            _player.vel.y = -PLAYER_NUDGE_VELOCITY;
        }
    }
}

void updateVelocity (float frametime)
{
    int yIn                 = (int) IsKeyDown (KEY_UP) - (int) IsKeyDown (KEY_DOWN);
    float magSqr            = Vector2LengthSqr (_player.vel);
    float mag               = sqrt (magSqr);
    Vector2 facingDirection = playerFacingDirection ();
    if (yIn > 0) {
        _player.vel = Vector2Add (_player.vel, Vector2Scale (facingDirection, PLAYER_ACCELERATION * frametime));
        if (mag > PLAYER_SPEED) {
            _player.vel = Vector2Scale (_player.vel, PLAYER_SPEED / mag);
        }
    } else {
        if (mag > 0) {
            float xSign = (_player.vel.x < 0) ? -1.0f : 1.0f;
            float ySign = (_player.vel.y < 0) ? -1.0f : 1.0f;

            float xAbs = _player.vel.x * xSign;
            float yAbs = _player.vel.y * ySign;

            float xWeight = xAbs * xAbs / magSqr;
            float yWeight = yAbs * yAbs / magSqr;

            float xDecel = xWeight * PLAYER_DECELERATION * xSign * frametime;
            float yDecel = yWeight * PLAYER_DECELERATION * ySign * frametime;

            _player.vel.x = (xAbs > xDecel) ? _player.vel.x - xDecel : 0;
            _player.vel.y = (yAbs > yDecel) ? _player.vel.y - yDecel : 0;
        }
    }
}

void playerMove (void)
{
    float frametime = GetFrameTime ();

    if (_player.state != PLAYER_STUNNED && _player.state != PLAYER_DEAD) {
        updateAngle (frametime);
        updateVelocity (frametime);
    }

    _player.pos = Vector2Add (_player.pos, Vector2Scale (_player.vel, frametime));
    updateWrap ();
}

Vector2 playerFacingDirection (void)
{
    return Vector2Rotate ((Vector2) {0, -1}, _player.rotation * DEG2RAD);
}

static void onCollision (Enemy *enemy)
{
    const float playerNudgeMagnitude   = 200.0f;
    const float asteroidSpeedReduction = 0.4f;

    _player.health--;
    if (_player.health <= 0) {
        onDeath ();
        return;
    }

    playerSetState (PLAYER_STUNNED);

    Vector2 nudgeDirection = Vector2Normalize (Vector2Subtract (_player.pos, enemy->pos));
    _player.vel            = Vector2Scale (nudgeDirection, playerNudgeMagnitude);

    enemy->vel = Vector2Scale (enemy->vel, asteroidSpeedReduction);
}

void updatePlayer (void)
{
    tickState ();
    playerMove ();
    if (_player.state == PLAYER_STUNNED) {
        return;
    }
    float _time = GetTime ();
    if (IsKeyDown (KEY_SPACE)) {
        if (_time > _player.lastFireTime + PLAYER_FIRE_DELAY) {
            PlaySound (_sound);
            addProjectile (Vector2Add (_player.pos, Vector2Scale (playerFacingDirection (), PLAYER_PROJECTILE_OFFSET)),
                           _player.rotation);
            _player.lastFireTime = _time;
        }
    }
    if (_player.state == PLAYER_IFRAME) {
        return;
    }
    Enemy *enemies  = enemiesArray ();
    const int count = ENEMY_MAX;

    for (int i = 0; i < count; i++) {
        Enemy *enemy = enemies + i;

        if (! enemies->active) {
            continue;
        }

        if (! CheckCollisionCircles (_player.pos, PLAYER_RADIUS, enemy->pos, enemyRadius (enemy))) {
            continue;
        }

        onCollision (enemy);
        break;
    }
}
