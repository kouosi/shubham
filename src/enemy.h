#pragma once

#include <raylib.h>

typedef enum {
    ENEMY_SMALL  = 1,
    ENEMY_MEDIUM = 2,
    ENEMY_LARGE  = 4,
} enemySize;

#define ENEMY_ROT_SPEED_MIN 5
#define ENEMY_ROT_SPEED_MAX 240
#define ENEMY_SPEED_MIN     100
#define ENEMY_SPEED_MAX     225
#define ENEMY_LIFE          10.0f
#define ENEMY_BASE_SCORE    10

typedef struct {
    bool active;
    Vector2 pos;
    Vector2 vel;
    enemySize size;
    Texture2D *texture;
    float rotation;
    float rotationSpeed;
    float creationTime;
} Enemy;

void initEnemy (void);
Enemy createEnemy (Vector2 position, Vector2 velocity, enemySize size);
void enemyDraw (Enemy *enemy);
float enemyRadius (Enemy *enemy);
void addEnemy (Vector2 position, enemySize size, float speedMod, bool spawn);
void destroyEnemy (int index, float angle);
Vector2 getNextEnemyPosition (void);
int updateEnemys (void);
bool enemyUpdate (Enemy *enemy, float frametime, float time);
void drawEnemys (void);
Enemy *enemiesArray (void);
void resetEnemys (void);
Texture2D LoadTextureFromHeader (int width, int height, PixelFormat format, void *data);
