#include <raylib.h>
#include <stdio.h>
#include <string.h>

int main (int argc, char **argv)
{
    Wave wave        = {0};
    char output[100] = {0};
    if (argc <= 1) {
        fprintf (stderr, "Usages: wav2c [FILE]...\n");
        return 1;
    }
    for (int i = 1; i < argc; i++) {
        wave = LoadWave (argv[i]);
        if (! IsWaveReady (wave)) {
            fprintf (stderr, "Image is not ready\n");
            continue;
        }
        strcat (output, argv[i]);
        strcat (output, ".h");
        ExportWaveAsCode (wave, output);
        fprintf (stderr, "Exported as %s\n", output);
        memset (output, 0, sizeof (output));
    }
    return 0;
}
