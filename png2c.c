#include <raylib.h>
#include <stdio.h>
#include <string.h>

int main (int argc, char **argv)
{
    Image img        = {0};
    char output[100] = {0};
    if (argc <= 1) {
        fprintf (stderr, "Usages: png2c [FILE]...\n");
        return 1;
    }
    for (int i = 1; i < argc; i++) {
        img = LoadImage (argv[i]);
        if (! IsImageReady (img)) {
            fprintf (stderr, "Image is not ready\n");
            continue;
        }
        strcat (output, argv[i]);
        strcat (output, ".h");
        ExportImageAsCode (img, output);
        fprintf (stderr, "Exported as %s\n", output);
        memset (output, 0, sizeof (output));
    }
    return 0;
}
