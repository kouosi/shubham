CC = x86_64-w64-mingw32-gcc
CC ?= gcc
CC_FLAGS = -Wall -Wextra
CC_FLAGS_WIN = $(CC_FLAGS) -I./raylib/ -L./raylib/ -lraylib -lm -lopengl32 -lgdi32
LD_FLAGS = -lm -lraylib
CC_FILES = $(wildcard src/*.c)

all: png2c wav2c window

linux: png2c.c wav2c.c $(CC_FILES)
	$(CC) $(CC_FLAGS) -lraylib -o png2c png2c.c
	$(CC) $(CC_FLAGS) -lraylib -o wav2c wav2c.c
	./wav2c assets/*.wav
	./png2c assets/*.png
	$(CC) $(LD_FLAGS) $(CC_FLAGS) -DDEBUG -g -O3 -o game-$@ $(CC_FILES)
	$(CC) $(LD_FLAGS) $(CC_FLAGS) -O3 -o game-$@ $(CC_FILES)

window: $(CC_FILES)
	$(CC) -o png2c.exe png2c.c $(CC_FLAGS_WIN) -lwinmm
	$(CC) -o wav2c.exe wav2c.c $(CC_FLAGS_WIN) -lwinmm
	./wav2c.exe assets/*.wav
	./png2c.exe assets/*.png
	$(CC) -DDEBUG -g -o game-debug.exe $^ $(CC_FLAGS_WIN) -lwinmm
	$(CC) -O3 -o game-release.exe $^ $(CC_FLAGS_WIN) -lwinmm -mwindows

